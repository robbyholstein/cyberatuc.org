---
title: "NSA visit recap"
date: 2018-10-02 12:18:49
author: ~igyartojohn
description: Last month, Mr. Jason Armstrong from the National Security Agency brought an Enigma machine from World War II and gave a guest presentation at one of our meetings.
main_image:
  src: 2018-09-18-jason-armstrong-talking.jpg
  alt: Jason Armstrong asking a question to a room full of students
  hidden: true
---
Have you heard of the NSA?

{% include image.html type="focus-small" src="1950s-nsa-mona-lisa-poster.jpg" alt="1950s NSA poster showing Mona Lisa with the caption 'Mona only smiles about her secrets. Keep Smiling.'" caption="An NSA poster from the 1950s ([source](https://commons.wikimedia.org/wiki/File:2018-06_NSA_security_motivational_poster_6614849.jpg))" %}

The National Security Agency is an intelligence agency of the United States Department of Defense under the direction of the Director of National Intelligence. The NSA is responsible for global monitoring, collection, and processing of information and data for foreign intelligence and counter-intelligence purposes, specializing in a discipline known as signals intelligence. Signals intelligence is intelligence gathering by interception of signals, whether communications between people or from electronic signals not directly used in communication.

{% include image.html type="focus-small" src="2018-09-18-nsa-enigma-machine.jpg" alt="An old wooden enigma machine opened up, with the keyboard and rotors visible" caption="The enigma machine opened up" %}

The field of cryptography is the study of techniques for secure communication in the presence of third parties called adversaries. Radio signals were once the primary way for transmitting information around the globe. The problem with radio signals as a means of communication though was that anybody with the right equipment have the ability to intercept and listen in on the communication. This flaw was of great strategic importance during World War I and II. To combat this, many militaries around the world would use codes and scramble the messages so that what is heard is not exactly what was intended. The most famous implementation is called Morse Code. In tandem with cryptography was the development of the subfield called cryptanalysis. Cryptanalysis is used to breach cryptographic systems and gain access to the encrypted messages. Cryptanalysis also is concerned with the finding out as much about the message as possible without having access to the intended message. This is why governments and militaries collect so much seemingly unnecessary information. The goal is to put the pieces together like a puzzle.
The advancement in computer technology has caused the field of cryptography to grow exponentially. So much so that great investment is being made into the next generation of cybersecurity professionals.

{% include image.html src="2018-09-18-nsa-group-posed.jpg" alt="A classroom full of students smiling for the camera" caption="We had a great turnout for the NSA visit!" %}

The guest speaker for last week's Cyber@UC meeting was Mr. Jason Armstrong from the National Security Agency. He is a graduate of John Hopkins University with a master's degree in Computer Science, with a focus on Artificial Intelligence. Mr. Armstrong presented a lecture about the history of cryptography and its relation to national security. He also introduced some of the early techniques that were used before the advancements that were made during World War I and II as well as brought one of the Enigma Machines pictured above that was used to break the codes used by German Intelligence Services during the world wars.

We had a great crowd come out to see Mr. Armstrong speak and enjoy some Dibella's Subs. Big thanks to everyone who turned out!

The University of Cincinnati as well as Cyber@UC have a close relationship with the NSA. There are many scholarships and programs offered to UC students. One of which is the Department of Defense (DoD) SMART Scholarship Program which is an opportunity for students pursuing an undergraduate or graduate degree in STEM disciplines to receive a full scholarship and be gainfully employed by the Department of Defense upon degree completion. More opportunities can be found [on Prof. Franco's website](http://gauss.ececs.uc.edu/Courses/c5155/opps.html).

{% include image.html src="2018-09-18-jason-armstrong-talking.jpg" alt="Jason Armstrong asking a question to a room full of students" %}
