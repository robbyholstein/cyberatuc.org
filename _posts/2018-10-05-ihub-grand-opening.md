---
title: "Weekly Blog: iHub Grand Opening"
date: 2018-10-09 19:11:24
author: young2r2
main_image:
  src: 2018-10-05-ihub-west-campus-sculpture.jpg
  alt: DAAP's sculpture of UC West Campus
  caption: DAAP's sculpture of UC West Campus
---
Last week (Friday October 5th) was the grand opening for UC's Innovation Hub. What was an old Sears building has been repurposed to be a place for students and employers to collaborate. Many local companies will be part of the Innovation Hub such as GE, Cincinnati Insurance and many others. Representatives from these companies will be there to help students with school as well as careers. It is a very good place to network if you are looking for coop/job opportunities.

Now we'll get into the real juice of the Innovation Hub. On the first floor, there is a bunch of equipment available to students. This equipment can be used for fabrication of plastic, fabric, etching, and metal. There are 3D printers, laser cutters, and sewing machines for any project that my need physical parts. They also have a printing machine for any prints of 50" to infinity provided you have an infinite roll of paper. The printer can also handle stickers.

{% include image.html src="2018-10-05-ihub-3d-printers.jpg" alt="3D Printers in the iHub" %}

So you probably thinking _wow, this is a lot of cool stuff! I dunno how to use it_. You are in luck! They will have staff on site to help students use the equipment. They can help you one-on-one or you can choose to take a class to get trained. They have a lot of cool stuff available to us now and the best of it is that it's all nonprofit. The only costs are the cost of materials.

{% include image.html src="2018-10-05-ihub-stairwell.jpg" alt="The stairwell in the iHub" %}

Some other uses of the Hub is that it can make a great place to study (or even live! jk). There are many study spaces available as well as class rooms if they are not being used. Those classrooms can be reserved for events. The hub also has a place to cook! It seems that there is only a microwave with a complementary sink available to students. There is an oven and stove in a business office but that is off limits to my understanding.

## Lab Progress

We have made some progress on the network topology this week. The topology right now looks like several VLANs that segregate all the racks into their own separate network along with a VLAN setup for iDRAC and Firewall Interface to protect it from misuse from any unauthorized users. We have the topology design nearly complete and need folks who want experience in setting up VLANs on the Firewalls and the Network switches. Both which are Cisco manufactured.
