---
layout: page
title: Legal
permalink: /legal
---
This site's source code can be found at [gitlab.com/cyberatuc/cyberatuc.org](https://gitlab.com/cyberatuc/cyberatuc.org). The source code of the site is licensed under the MIT License. The text of the license is included here:

> MIT License
>
> Copyright (c) 2018 Hayden Schiff
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

All content on this website that is created by Cyber@UC members is freely licensed under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). However, please be aware that we sometimes use content from other sources that is not necessarily freely licensed. Typically, the original source will be explicitly credited, or the source of the content will otherwise be readily apparent. Please feel free to contact us at [{{ site.email }}](mailto:{{ site.email }}) to clarify the source of any particular content.

The photo on the home page is "[reflection of McMicken Hall in the University Pavilion](https://www.flickr.com/photos/fusion_of_horizons/1534739047)", &copy; 2007 [fusion-of-horizons](https://www.flickr.com/people/fusion_of_horizons/), [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) license
