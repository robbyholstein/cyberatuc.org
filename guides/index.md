---
layout: page
title: Guides
permalink: /guides/
---
This page is for hosting tutorials, guides, and other helpful information. It is still under construction, so check back soon!

## Tutorials
* VirtualBox videos
    * [Part 1](https://www.youtube.com/watch?v=miVb-hCkAEA)
    * [Part 2](https://www.youtube.com/watch?v=eTsuo5KJi-M)
* [Linux: Mounting Drives]({{ site.url }}/guides/mounting-drives)

## Cyber@UC meta topics
* [Contributing to cyberatuc.org]({{ site.url }}/guides/website)
* [Running the Cyber@UC livestream]({{ site.url }}/guides/livestream)
