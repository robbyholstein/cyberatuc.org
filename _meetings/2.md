---
title: "Committees, Leadership, and Goals"
date: 1900-01-01 18:30:00
meeting_number: 2
---
This week, we laid out the leadership structure for the club, naming the current officers and establishing four committees: Social Media, Content, Event, and Fundraising. We also discussed goals for the clubs, including fundraising via a bake sale.
